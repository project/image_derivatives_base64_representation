<?php

namespace Drupal\image_derivatives_base64_representation\Plugin\DataType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\image\Plugin\DataType\ImageStyleDerivativesDefinition;

/**
 * @inheritdoc
 */
class EnhancedImageStyleDerivativesDefinition extends ImageStyleDerivativesDefinition {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    parent::getPropertyDefinitions();

    $this->propertyDefinitions = array_map(function ($image_style_definition) {
      /** @var \Drupal\Core\TypedData\MapDataDefinition */
      return $image_style_definition->setPropertyDefinition('base64', DataDefinition::create('string'));
    }, $this->propertyDefinitions);

    return $this->propertyDefinitions;
  }

}
