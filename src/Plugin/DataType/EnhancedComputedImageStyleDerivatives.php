<?php

namespace Drupal\image_derivatives_base64_representation\Plugin\DataType;

use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\file\FileInterface;
use Drupal\image\ImageStyleInterface;
use Drupal\image\Plugin\DataType\ComputedImageStyleDerivatives;
use Drupal\image_derivatives_base64_representation\Plugin\DataType\EnhancedImageStyleDerivativesDefinition;

/**
 * Computed image style derivatives class w/ optional base64 encoded representation.
 *
 * @ingroup typed_data
 * @see \Drupal\image\Entity\ImageStyle
 *
 * @DataType(
 *   id = "image_style_derivatives",
 *   label = @Translation("Enhanced image style derivatives"),
 *   definition_class = "\Drupal\image_derivatives_base64_representation\Plugin\DataType\EnhancedImageStyleDerivativesDefinition",
 * )
 */
class EnhancedComputedImageStyleDerivatives extends ComputedImageStyleDerivatives {

  public static function createInstance($definition, $name = NULL, TraversableTypedDataInterface $parent = NULL) {
    $enhanced_definition = new EnhancedImageStyleDerivativesDefinition([
      'type' => $definition->getDataType(),
      'computed' => $definition->isComputed(),
      'internal' => $definition->isInternal(),
      'label' => $definition->getLabel(),
      'description' => $definition->getDescription(),
    ]);

    return parent::createInstance($enhanced_definition, $name, $parent);
  }

  /**
   * @param \Drupal\file\FileInterface $file
   * @param $width
   * @param $height
   * @param \Drupal\image\ImageStyleInterface $style
   * @return array|bool
   *
   * @todo rather than returning an array, return a value object that
   *   implements CacheableDependencyInterface
   *
   * @see \Drupal\image\Entity\ImageStyle::buildUrl
   * -> tag: config:image.settings
   *
   * -> tag: $style->getCacheTags()
   */
  protected function computeImageStyleMetadata(FileInterface $file, $width, $height, ImageStyleInterface $style) {

    $metadata = parent::computeImageStyleMetadata($file, $width, $height, $style);

    if ($style->get('base64') && isset($metadata['url'])) {

      $file_uri = $file->getFileUri();
      $derivative_uri = $style->buildUri($file_uri);
      if (!file_exists($derivative_uri)) {
        $style->createDerivative($file_uri, $derivative_uri);
      }
      $derivative_file = file_get_contents($derivative_uri);

      $metadata['base64'] = 'data:image/jpeg;base64,' . base64_encode($derivative_file);
    }

    return $metadata;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $value = parent::getValue();
    return $value ? $value : [];
  }

}
